// ==UserScript==
// @name     4chan2reddit
// @grant GM.getValue
// @grant GM.setValue
// @version  1
// @description Destroy free speech and rely on the discernment of children.
// @include      *://boards.4chan.org/*
// @include      *://boards.4channel.org/*
// ==/UserScript==

// The script will read posts and apply rules when it matches filters and #hashtags.
const upvoteFilter = /(?<!\bno.*)(up.?(vot|doot|boat)|t ?h ?a ?n ?k|epic|\banon|g[ou]+d|\bamaz[ie]|desu|\bbelie|\bapprov|the best|senpai|\(you\)|gigachad|\bpost|fri?en|haha|bump|s(au|our)ce\b|good ?job|\blove|\bliked|based|\bagree|nice|cool|great|cute|qt|lmao|kek|patrician|pbp\b|lol\b|\bhi\b|\bhey\b|\bhello\b|\bhow are (yo)?u|\blost\b)/i
const downvoteFilter = /(?<!\bno.*)(\bbad\b|\bshiggy\b|\bishygddt\b|down.?(vot|doot|boat)|\bkys\b|\bhell|skill issue|disapprov|touch.*grass|simps?\b|rekt|frog|trash|disagree|onion|s[o0][iy]|\b(cringe|seethe|cope)\b|f ?[a] ?g|gay|(pl|r).(dd|bb).t|kil+ yoursel|\bI.*hate|degenerate|tard|nig(s|ger)|j[e]w|\bb(8|ait)\b|schizo|shut.*up|stfu|rent.?free|cancer|\bloser)/i
const tagSearchQuery = /[^0-9a-z_+/]#([0-9a-z_]{2,})/gi
const selfTagQuery = /\[([^\]]+?)\]/gi
const goldFilter = /golds?\b/i
function voteAndTag(reply, replyText, correspondent, virtualOpReply) {
  if (!correspondent || !replyText) return
  replyText = ` ${replyText} `
  const tags = replyText.match(tagSearchQuery)?.map(t => t.trim().slice(1))
  const selfTags = replyText.match(selfTagQuery)?.map(t => t.slice(1, -1).trim())
  if (correspondent !== reply) {
    // Only tabulate a post as an upvote or downvote if it is unambiguous
    const upvoted = replyText.match(upvoteFilter)
    const downvoted = replyText.match(downvoteFilter)
    if (upvoted && !downvoted && !correspondent.upvoters.includes(reply.id.substring(1))) correspondent.upvoters.push(reply.id.substring(1))
    if (downvoted && !upvoted && !correspondent.downvoters.includes(reply.id.substring(1))) correspondent.downvoters.push(reply.id.substring(1))
  }
  if (!virtualOpReply) {
    for (let i = 0; i < tags?.length; i++) {
      const tagName = tags[i].toLowerCase().replace(/_/g, '')
      const tag = correspondent.tags[tagName] || (correspondent.tags[tagName] = { tag: tags[i], count: 0, color: generateRandomColor() })
      tag.count++
    }
  }
  // Treat tags as self-tags if they are not explicitly quoting OP
  if (selfTags || virtualOpReply && tags) for (const tagInfo of virtualOpReply ? (selfTags || []).concat(tags || []) : selfTags) {
    const tagName = tagInfo.toLowerCase().replace(/[- /]/g, '')
    let tag = reply.tags[tagName]
    if (tag) {
      if (tag.tag.length > tagInfo.length + 2) tag.tag = tagInfo
    } else tag = reply.tags[tagName] = { tag: tagInfo, count: 0, color: generateRandomColor() }
    tag.count++
  }
}
// You must post in order to tag and vote.
function writeVote(id, score) {
  const postQuote = `>>${id}`
  Array.from(document.getElementsByTagName('textArea')).forEach((t) => (t.value += (t.value.includes(postQuote) ? ' ' : postQuote + '\n') + score))
}
const generateRandomColor = () => `rgb(${['r', 'g', 'b'].map(() => Math.floor((Math.random() * 256 + 255) / 2)).join(',')})`
function updateScores() {
  storeUserPreferences()
  const posts = Array.from(document.getElementsByClassName('postMessage')).filter(p => !p.id.includes('_')).map((p) => {
    if (p.tags) Object.values(p.tags).forEach((t) => (t.count = 0))
    else {
      p.tags = {}
      p.upvoters = []
      p.downvoters = []
    }
    return p
  })
  // Posts are upvoted and downvoted by replies, so tabulate votes by most recent
  for (const post of posts.reverse()) {
    // Parse replies to posts into votes and tags.
    const replyContainer = post.parentElement.parentElement
    const threadContainer = replyContainer.parentElement
    const op = threadContainer.getElementsByClassName('postMessage')[0]
    let replyTo = {}
    let correspondentGroup = []
    let virtualOpReply = false
    for (const postElement of post.childNodes) {
      // Segment replies by quote links
      if (postElement.className?.startsWith('quotelink')) {
        if (correspondentGroup.filter(c => replyTo[c]).length) correspondentGroup = []
        let correspondentId = postElement.hash.substring(2)
        correspondentGroup.push(correspondentId)
        if (!replyTo[correspondentId]) replyTo[correspondentId] = ''
      } else if (postElement.data)
        if (correspondentGroup.length) correspondentGroup.forEach((correspondent) => (replyTo[correspondent] += postElement.data + ' '))
        else {
          // Posters reply to OP by default.
          let opId = op.id.substring(1)
          correspondentGroup = [opId]
          if (!replyTo[opId]) {
            replyTo[opId] = postElement.data + ' '
            virtualOpReply = true
          }
        }
    }
    Object.entries(replyTo).forEach(([correspondent, replyText]) => voteAndTag(post, replyText, posts.find((p) => p.id.substring(1) === correspondent), virtualOpReply))
    // Posts outside the user's thresholds will be hidden.
    const voteScore = post.upvoters.length - post.downvoters.length
    const container = !post.parentElement.classList.contains('op') ? replyContainer : threadContainer
    if (voteScore >= parseInt(thresholdMinPreference) && voteScore <= parseInt(thresholdMaxPreference)) {
      container.classList.remove('hide')
      // Individual posts will add a vote total and a tag cloud.
      let total = document.getElementById('total' + post.id.substring(1))
      let votes = document.getElementById('votes' + post.id.substring(1))
      if (!total) {
        votes = document.createElement('div')
        votes.id = 'votes' + post.id.substring(1)
        votes.innerHTML = ' '
        votes.className = 'totals'
        post.appendChild(votes)
        for (const [modifier, color, label] of [['upvoted', '0f0', '⬆'], ['downvoted', 'f00', '⬇']]) {
          const vote = document.createElement('a')
          vote.addEventListener('click', () => writeVote(post.id.substring(1), modifier))
          vote.style = `display:inline;color:#${color};font-weight:bolder;background-color:#888`
          vote.innerHTML = `&nbsp;${label}&nbsp;`
          votes.appendChild(vote)
        }
        total = document.createElement('div')
        total.id = 'total' + post.id.substring(1)
        total.style = 'display:inline;color:#222'
        votes.appendChild(total)
      }
      votes.style = 'width:40%' + (voteScore > 0 ? ';background-color:#8f8' : voteScore < 0 ? ';background-color:#f88' : '')
      total.innerHTML = '&nbsp;' + voteScore
      Object.values(post.tags).forEach((t) => {
        let tag = document.getElementById('tag' + post.id.substring(1) + t.tag)
        if (!tag) {
          tag = document.createElement('div')
          tag.id = 'tag' + post.id.substring(1) + t.tag
          tag.style = 'display:inline;color:#222;background-color:' + t.color
          post.appendChild(tag)
          post.innerHTML += ' '
        }
        tag.innerHTML = decodeURIComponent(t.tag) + (t.count > 1 ? ' x' + t.count : '')
      })
      if (post.innerText.match(goldFilter)) {
        post.parentElement.style.backgroundColor = "#e1e114";
      }
    } else if (container === replyContainer || document.body.classList.contains('is_index')) container.classList.add('hide')
  }
}
// User preferences will be loaded.
const defaultMin = '-10'
const defaultMax = '100'
var thresholdMinPreference = defaultMin
var thresholdMaxPreference = defaultMax
GM.getValue('minThreshold', defaultMin).then(async (v) => {
  thresholdMinPreference = v || defaultMin
  thresholdMaxPreference = (await GM.getValue('maxThreshold', defaultMax)) || defaultMax
  updateScores()
  setTimeout(() => {
    const navLinkContainers = document.getElementsByClassName('navLinks')
    const style = document.createElement('style')
    style.innerHTML = '.hide, .hide + hr { display: none }'
    document.body.appendChild(style)
    for (let i = 0; i < navLinkContainers.length; i++) {
      const mainContainer = document.createElement('div')
      mainContainer.style = 'display:inline'
      if (i < 2) {
        // Threshold Min/Max textboxes will be added to the thread.
        for (const [type, preference, label] of [['Min', thresholdMinPreference, 'Minimum score'], ['Max', thresholdMaxPreference]]) {
          const threshold = document.createElement('input')
          threshold.className = 'threshold' + type
          threshold.type = 'number'
          threshold.value = preference
          const thresholdLabel = document.createElement('label')
          thresholdLabel.for = threshold.id
          thresholdLabel.innerHTML = label || type
          mainContainer.innerHTML += ' '
          mainContainer.appendChild(thresholdLabel)
          mainContainer.innerHTML += ' '
          mainContainer.appendChild(threshold)
        }
      }
      navLinkContainers[i].appendChild(mainContainer)
    }
    updateScores()
    setInterval(updateScores, 2000)
  }, 4000)
})
function storeUserPreferences() {
  const thresholdMin = Array.from(document.getElementsByClassName('thresholdMin'))
  const thresholdMax = Array.from(document.getElementsByClassName('thresholdMax'))
  const newMin = thresholdMin?.length && thresholdMin.find((t) => t.value !== thresholdMinPreference)?.value
  if (newMin) GM.setValue('minThreshold', thresholdMinPreference = newMin || thresholdMinPreference || defaultMin)
  if (parseInt(thresholdMinPreference) > parseInt(thresholdMaxPreference)) GM.setValue('maxThreshold', thresholdMaxPreference = thresholdMinPreference)
  else {
    const newMax = thresholdMax?.length && thresholdMax.find((t) => t.value !== thresholdMaxPreference)?.value
    if (newMax) GM.setValue('maxThreshold', thresholdMaxPreference = newMax || thresholdMaxPreference || defaultMax)
    if (parseInt(thresholdMinPreference) > parseInt(thresholdMaxPreference)) GM.setValue('minThreshold', thresholdMinPreference = thresholdMaxPreference)
  }
  thresholdMax.forEach((t) => (t.value = thresholdMaxPreference))
  thresholdMin.forEach((t) => (t.value = thresholdMinPreference))
}